@extends('app')
@section('title', 'Новости ')

@section('content')

<h1 class=" text-center">Новости</h1>

<div class="container">
    <div class="row news-list">
        @foreach($news as $new)
            <div class="box-news col
                      @if ($i == 0)
            {{$i++}}
                shadow-red
@elseif ( $i == 2)
            {{ $i = 0 }}
                shadow-purple
@else
            {{$i++}}
            @endif
                ">
                <form method="get" action="{{route('show', $new)}}">
                <div class="content-news">
                    <small class="date text-center">{{$new->created_at}}</small>
                    <p class="font-weight-bold col-lg-12 col-md news-text">{{$new->header}}</p>
                    <button class="btnnews" >Подробнее</button>
                </div>
                </form>
            </div>
        @endforeach
    </div>
</div>
</div>
<div class="pag mx-4 my-5"> {{$news->links("pagination::bootstrap-4")}}</div>


</div>
<a class="nav-link btn btn-danger float-right mx-4 col-5" href="{{route('create')}}">Добавить Новость <span class="sr-only">(current)</span></a>


@endsection
