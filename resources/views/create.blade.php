@extends('app')

@section('title', 'Создать новость ')

@section('content')

   <div class="container">
       <form action="{{route('store')}}" method="POST">
           @csrf
           <div class="form-group">
               <label for="header">Введите заголовок</label>
               <input type="text" class="form-control" name="header" aria-describedby="emailHelp" >
           </div>
           <div class="form-group">
               <label for="description">Описание новости</label>
               <textarea type="text" class="form-control" cols="100"  name="description" ></textarea>
           </div>

           <button type="submit" class="btn btn-danger float-right">Добавить новость</button>
       </form>

   </div>

@endsection
