@extends('app')
@section('title', 'Новость ')


@section('content')

    <div class="container">
        <div class="name"><h1>{{$news->header}}</h1></div>
        <div class="description"><p>{{$news->description}}</p></div>
        <small class="date text-center">{{$news->created_at}}</small>
        <div class="col-12">

            <a class="btn btn-danger float-right" href="/"> На Главную</a>
        </div>

    </div>
@endsection
