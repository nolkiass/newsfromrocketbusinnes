
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">



    <link rel="stylesheet" href="/css/app.css">



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <title>@yield('title')</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light ">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="logo col "> <img src="/img/logo.png"></div>
    <div class="collapse navbar-collapse " id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ali col-lg-6 bar" >
            <li class="nav-item">
                <a class="nav-link" href="#">Услуги <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Партфолио <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Вакансии <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Контакты <span class="sr-only">(current)</span></a>
            </li>
            <li>
                <div class="info col-lg-2 d-md-none "><div class="font-weight-bold">8(863)243-15-10</div> Росов-на-Дону</div>
            </li>
        </ul>
    </div>
    <div class="info col-lg-2 d-none d-lg-block"><div class="font-weight-bold">8(863)243-15-10</div> Росов-на-Дону</div>
</nav>
<div class="main">
    <img class="col-lg-12 main-img" src="/img/Rectangle.png" alt="">
    <div class="container">
        <div class="content">
            <h2 class="col-lg-5">Рекламно-информационное агенство</h2>
            <p class="col-lg-5">Будем рады, если Вы обратитесь в наше Агентство. Мы готовы предложить Вам передовые решения для продвижения Вашего бизнеса.</p>
            <form action="#">
                <input class="inp" value="Номер телефона">
                <button class="btn btn-danger but">Обратный звонок</button>
            </form>
        </div>
    </div>
</div>

@yield('content')


<div class="footer row">
    <div class="logo-f col-2"><img src="/img/logoend.png" alt=""></div>
    <div class="col-5 footbar row">
        <p><a class="col" href="#">Услуги <span class="sr-only">(current)</span></a></p>
        <p><a class="col" href="#">Партфолио <span class="sr-only">(current)</span></a></p>
        <p><a class="col" href="#">Отзывы <span class="sr-only">(current)</span></a></p>
        <p><a class="col" href="#">Вакансии <span class="sr-only">(current)</span></a></p>
        <p><a class="col" href="#">Контакты <span class="sr-only">(current)</span></a></p>
    </div>
</div>


</body>
</html>
