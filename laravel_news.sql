-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 03 2020 г., 18:09
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `laravel_news`
--

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_31_153418_create_news_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `created_at`, `updated_at`, `header`, `description`, `active`) VALUES
(0, '2020-11-02 20:35:32', NULL, 'Тестовая новость 1 ', 'created_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_atcreated_at', 1),
(1, '2020-11-02 20:36:39', NULL, 'Акции на размещение баннерного панно 20% до конца июля', 'Акции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июляАкции на размещение баннерного панно 20% до конца июля', 0),
(2, '2020-11-02 20:40:00', NULL, 'Мы работаем в обычном режиме с 15 июня!', 'Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!Мы работаем в обычном режиме с 15 июня!', 0),
(3, '2020-11-02 20:40:00', NULL, 'Как правильно подобрать рекламный канал?', 'Как правильно подобрать рекламный канал?Как правильно подобрать рекламный канал?Как правильно подобрать рекламный канал?Как правильно подобрать рекламный канал?', 0),
(5, '2020-11-03 10:35:00', '2020-11-03 10:35:00', 'Том Харди сыграет в драме о войне во Вьетнаме', 'Том Харди сыграет одну из главных ролей в драме под названием \"The Things They Carried\", события которой будут происходить во время войны во Вьетнаме. По сведениям издания Deadline, постановщиком назначен создатель сказки \"Белоснежка и охотник\" Руперт Сандерс.\r\n\r\nВ картине также будут сниматься Тай Шеридан, Билл Скарсгард, Пит Дэвидсон, Эштон Сандерс и другие актеры. Сценарий будет основан на серии рассказов Тима О`Брайана, посвященных взводу молодых солдат и их жизни на передовой. По мотивам произведения ранее ставились театральные спектакли, а в 1998 году часть его была экранизирована в виде фильма \"Солдатская любовь\".\r\n\r\n\"Это прекрасное произведение и одна из самых запоминающихся книг, которые я когда-либо читал. Для меня она выходит за рамки темы молодых людей на войне и исследует ландшафт глубоких человеческих эмоций, которые живут внутри каждого из нас. Мы живем в такие неспокойные времена, и темы любви, страха и смертности, которые Тим исследовал тридцать лет назад, все еще резонируют сегодня, возможно, даже более мощно. Я так горжусь актерским составом, который мы собрали для этого проекта\", - говорится в заявлении Сандерса. Сроки работ над проектом пока не уточняются.', 0),
(6, '2020-11-03 10:47:44', '2020-11-03 10:47:44', 'Премьера трейлера сериала \"Основание\" по Айзеку Азимову', 'Издание \"Новости кино\" представляет первый трейлер сериала \"Основание\", который базируется на знаменитом цикле произведений американского писателя-фантаста Айзека Азимова. Главные роли исполнили Джаред Харрис, Ли Пейс, Джеффри Кантор и другие артисты. Пилотный эпизод шоу, которое представит зрителям потоковый сервис Apple TV+, снял режиссер Руперт Сандерс.\r\n\r\nНапомним, что цикл \"Основание\" был представлен в 40-х годах прошлого века. Речь в нем идет о попытке спасти человеческую цивилизацию и знания, которую предпринимает группа ученых психоисториков во главе с гениальным математиком Хари Селдоном, предсказавшим разрушение Галактической империи и тысячелетний хаос. Селдон собирает лучшие умы Империи и переселяет их на мрачную планету на краю Галактики, которая должна служить маяком надежды для будущих поколений. Он называет свое святилище Основанием.\r\n\r\nПопытки экранизации \"Основания\" неоднократно предпринимались и ранее. Так, в 2008 году студия Columbia Pictures намеревалась поручить съемки фильма режиссеру Роланду Эммериху. В 2014 году права приобрели HBO и Warner Bros. TV. Но все эти проекты так и не были реализованы.', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
